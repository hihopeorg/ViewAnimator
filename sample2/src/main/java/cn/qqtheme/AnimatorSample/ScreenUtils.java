package cn.qqtheme.AnimatorSample;


import ohos.aafwk.ability.Ability;
import ohos.agp.window.service.DisplayAttributes;
import ohos.agp.window.service.DisplayManager;
import ohos.agp.window.service.Window;
import ohos.agp.window.service.WindowManager;
import ohos.app.Context;

/**
 * Screen utility
 * <p>
 * Author:李玉江[QQ:1032694760]
 * DateTime:2015/1/16
 */
public final class ScreenUtils {
    private static boolean isFullScreen = false;

    /**
     * Display metrics display metrics.
     *
     * @param context the context
     * @return the display metrics
     */
    public static DisplayAttributes displayMetrics(Context context) {
        DisplayAttributes dm = DisplayManager.getInstance().getDefaultDisplay(context).get().getAttributes();
        //LogUtils.debug("screen width=" + dm.widthPixels + "px, screen height=" + dm.heightPixels
        //        + "px, densityDpi=" + dm.densityDpi + ", density=" + dm.density);
        return dm;
    }

    /**
     * Width pixels int.
     *
     * @param context the context
     * @return the int
     */
    public static int widthPixels(Context context) {
        return displayMetrics(context).width;
    }

    /**
     * Height pixels int.
     *
     * @param context the context
     * @return the int
     */
    public static int heightPixels(Context context) {
        return displayMetrics(context).height;
    }

    /**
     * Density float.
     *
     * @param context the context
     * @return the float
     */
    public static float density(Context context) {
        return displayMetrics(context).densityPixels;
    }

    /**
     * Density dpi int.
     *
     * @param context the context
     * @return the int
     */
    public static int densityDpi(Context context) {
        return displayMetrics(context).densityDpi;
    }

    /**
     * Is full screen boolean.
     *
     * @return the boolean
     */
    public static boolean isFullScreen() {
        return isFullScreen;
    }

    /**
     * Toggle full displayMetrics.
     *
     * @param activity the activity
     */
    public static void toggleFullScreen(Ability activity) {
        Window window = activity.getWindow();
        int flagFullscreen = WindowManager.LayoutConfig.MARK_FORCE_FULLSCREEN_IMPOSSIBLE;
        if (isFullScreen) {
            window.clearFlags(flagFullscreen);
            isFullScreen = false;
        } else {
            window.addWindowFlags(flagFullscreen);
            isFullScreen = true;
        }
    }

    /**
     * Keep bright.
     *
     * @param activity the activity
     */
    public static void keepBright(Ability activity) {
        //需在setContentView前调用
        int keepScreenOn = WindowManager.LayoutConfig.MARK_LOCK_AS_SCREEN_ON;
        activity.getWindow().addWindowFlags(keepScreenOn);
    }

}
