package cn.qqtheme.AnimatorSample.slice;


import cn.qqtheme.AnimatorSample.ResourceTable;

import com.github.florent37.viewanimator.AnimationListener;
import com.github.florent37.viewanimator.ViewAnimator;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.animation.Animator;
import ohos.agp.components.Component;
import ohos.agp.components.Image;

public class WelcomeAbilitySlice extends AbilitySlice {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_welcome);
        Image image = (Image) findComponentById(ResourceTable.Id_welcome_heart);

        image.setBindStateChangedListener(new Component.BindStateChangedListener() {
            @Override
            public void onComponentBoundToWindow(Component component) {
                ViewAnimator.animate(image).fall().fadeIn().singleInterpolator(Animator.CurveType.DECELERATE).onStop(new AnimationListener.Stop() {
                    @Override
                    public void onStop() {
                        Intent secondIntent = new Intent();
                        // 指定待启动FA的bundleName和abilityName
                        Operation operation = new Intent.OperationBuilder()
                                .withDeviceId("")
                                .withBundleName("cn.qqtheme.AnimatorSample")
                                .withAbilityName("cn.qqtheme.AnimatorSample.MainAbility")
                                .build();
                        secondIntent.setOperation(operation);
                        // 通过AbilitySlice的startAbility接口实现启动另一个页面
                        startAbility(secondIntent);
                    }

                }).start();
            }

            @Override
            public void onComponentUnboundFromWindow(Component component) {

            }
        });
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
