package cn.qqtheme.AnimatorSample.slice;

import cn.qqtheme.AnimatorSample.*;

import cn.qqtheme.AnimatorSample.PathUtils;
import cn.qqtheme.AnimatorSample.ScreenUtils;
import cn.qqtheme.AnimatorSample.SvgPathParser;
import com.github.florent37.viewanimator.AnimationBuilder;
import com.github.florent37.viewanimator.AnimationListener;
import com.github.florent37.viewanimator.ViewAnimator;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorGroup;
import ohos.agp.animation.AnimatorProperty;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.*;
import ohos.agp.render.Path;
import ohos.agp.render.render3d.ViewHolder;
import ohos.agp.window.dialog.CommonDialog;
import ohos.agp.window.dialog.IDialog;
import ohos.agp.window.service.Display;
import ohos.agp.window.service.DisplayAttributes;
import ohos.agp.window.service.DisplayManager;
import ohos.agp.window.service.Window;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

import java.util.ArrayList;
import java.util.Optional;
import java.util.Timer;
import java.util.TimerTask;

public class MainAbilitySlice extends AbilitySlice implements Component.ClickedListener {

    int windowHeight, windowWidth;
    Image imageView, imageView4path, imageView4svg, imageView4follower;
    ComponentContainer bubbleLayout;
    private static final String SVG_PATH = "m 182.89072,369.33798 0,0 c 0,0 -5.74786,-42.19512 " +
            "-11.46215,-52.19512 -5.71428,-10 -7.14286,-14.28572 -12.85714,-20 -5.71429,-5.71429 " +
            "-8.57143,-11.42857 -15.71429,-15.71429 -7.14285,-4.28571 -17.14285,-8.57143 -17.14285," +
            "-8.57143 0,0 -12.85715,-8.57143 -20,-12.85714 C 98.571429,255.71429 90,252.85714 " +
            "84.285714,248.57143 78.571429,244.28571 67.142857,230 67.142857,230 c 0,0 -7.142857," +
            "-5.71429 -8.571428,-14.28571 -1.428572,-8.57143 -5.714286,-22.85715 -5.714286,-22.85715 " +
            "l -1.428572,-17.14285 0,-18.57143 L 70,140 85.714286,137.14286 114.28571,138.57143 " +
            "137.14286,150 160,167.14286 l 15.71429,5.71428 12.85714,2.85715 15.71428,1.42857 " +
            "10,0 c 0,0 10,-1.42857 17.14286,-5.71429 C 238.57143,167.14286 245.71429,160 245.71429," +
            "160 l 21.42857,-15.71429 c 0,0 4.28571,-5.71428 10,-5.71428 5.71428,0 14.28571,0 " +
            "18.57143,0 4.28571,0 15.71428,2.85714 15.71428,2.85714 l 11.42857,14.28572 c 0,0 4.28572," +
            "10 4.28572,18.57142 0,8.57143 -8.57143,21.42858 -12.85715,28.57143 -4.28571,7.14286 " +
            "-18.57142,20 -18.57142,20 l -17.14286,10 c 0,0 -8.57143,-5.71428 -17.14286,11.42857 " +
            "-8.57143,17.14286 -20,28.57143 -20,28.57143 L 230,294.28571 215.71429,312.85714 c 0,0 " +
            "-4.28572,4.28572 -8.57143,15.71429 -4.28572,11.42857 -24.25214,40.76655 -24.25214," +
            "40.76655 l 0,0 c 0,0 0,41.25 0,41.25 0,0 2.89071,-29.41202 0,0 C 180,440 171.42857," +
            "448.57143 171.42857,461.42857 c 0,12.85714 14.28572,-4.28571 -4.28571,30 -18.57143," +
            "34.28572 -15.71429,25.71429 -27.14286,41.42857 -11.42857,15.71429 -27.14286,20 " +
            "-27.14286,20 l -29.999997,-15.71428 -20,-41.42857 1.428571,-38.57143 c 0,0 -2.857143," +
            "-8.57143 5.714286,-11.42857 8.571429,-2.85715 25.714286,-10 25.714286,-10 l 31.428574," +
            "-1.42858 c 0,0 18.57143,2.85715 28.57143,2.85715 10,0 32.85714,1.42857 40,0 7.14285," +
            "-1.42857 30.92643,-26.55488 30.92643,-26.55488 0,0 33.35928,-4.87369 37.64499,-6.30227 " +
            "4.28572,-1.42857 20,-21.42857 20,-21.42857 L 300,351.42857 300,340 l -5.71429,-18.57143 " +
            "-18.57142,-10 -24.28572,5.71429 L 232.85714,340 c 0,0 -6.21642,29.33798 -6.21642," +
            "29.33798 l -43.75,41.25 c 0,0 -15.68072,-27.98344 0,0 15.68071,27.98345 28.53785,56." +
            "55488 28.53785,56.55488 0,0 22.85714,10 32.85714,17.14285 10,7.14286 8.57143,5.71429 " +
            "12.85715,14.28572 4.28571,8.57143 5.71428,24.28571 5.71428,37.14286 0,12.85714 " +
            "-12.85714,28.57142 -12.85714,28.57142 0,0 -34.28571,8.57143 -47.14286,5.71429 " +
            "-12.85714,-2.85714 -35.71428,-14.28571 -40,-22.85714 -4.28571,-8.57143 -18.57143," +
            "-51.42857 -18.57143,-60 0,-8.57143 1.42858,-40 1.42858,-40 l -4.28572,-42.85715 c " +
            "0,0 5.71429,4.28572 -5.71428,-11.42857 C 124.28571,377.14286 118.57143,368.57143 " +
            "112.85714,364.28571 107.14286,360 82.857143,342.85714 75.714286,341.42857 68.571429,340 " +
            "47.142857,340 47.142857,340 c 0,0 -8.571428,-4.28571 -8.571428,11.42857 0,15.71429 0,30 " +
            "7.142857,37.14286 7.142857,7.14286 22.857143,17.14286 22.857143,17.14286 0,0 18.571428," +
            "2.85714 28.571428,1.42857 10.000003,-1.42857 34.285713,-8.57143 34.285713,-8.57143 l " +
            "51.46215,12.01655 0,0 0,-41.25";

    private static final int[] START_POINT = new int[]{300, 270};
    private static final int[] BOTTOM_POINT = new int[]{300, 400};
    private static final int[] LEFT_CONTROL_POINT = new int[]{450, 200};
    private static final int[] RIGHT_CONTROL_POINT = new int[]{150, 200};
    private DisplayAttributes metrics;
    HiLogLabel label = new HiLogLabel(HiLog.INFO, 0x677793, "MainAbilitySlice");

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        metrics = ScreenUtils.displayMetrics(this);
        windowHeight = AttrHelper.vp2px(getResourceManager().getDeviceCapability().height, getContext());
        windowWidth = AttrHelper.vp2px(getResourceManager().getDeviceCapability().width, getContext());
        bubbleLayout = (ComponentContainer) findComponentById(ResourceTable.Id_animation_heart);
        imageView = (Image) findComponentById(ResourceTable.Id_animation_image);
        imageView4path = (Image) findComponentById(ResourceTable.Id_animation_path);
        imageView4svg = (Image) findComponentById(ResourceTable.Id_animation_svg_path);
        imageView4follower = (Image) findComponentById(ResourceTable.Id_animation_snow);
        findComponentById(ResourceTable.Id_on_dialog).setClickedListener(this);
        findComponentById(ResourceTable.Id_on_shake).setClickedListener(this);
        findComponentById(ResourceTable.Id_on_bounce).setClickedListener(this);
        findComponentById(ResourceTable.Id_on_bounce_in).setClickedListener(this);
        findComponentById(ResourceTable.Id_on_flash).setClickedListener(this);
        findComponentById(ResourceTable.Id_on_flip_orizontal).setClickedListener(this);
        findComponentById(ResourceTable.Id_on_flip_vertical).setClickedListener(this);
        findComponentById(ResourceTable.Id_on_wave).setClickedListener(this);
        findComponentById(ResourceTable.Id_on_tada).setClickedListener(this);
        findComponentById(ResourceTable.Id_on_pulse).setClickedListener(this);
        findComponentById(ResourceTable.Id_on_stand_up).setClickedListener(this);
        findComponentById(ResourceTable.Id_on_swing).setClickedListener(this);
        findComponentById(ResourceTable.Id_on_wobble).setClickedListener(this);
        findComponentById(ResourceTable.Id_on_fall).setClickedListener(this);
        findComponentById(ResourceTable.Id_on_news_paper).setClickedListener(this);
        findComponentById(ResourceTable.Id_on_slit).setClickedListener(this);
        findComponentById(ResourceTable.Id_on_slide_left_in).setClickedListener(this);
        findComponentById(ResourceTable.Id_on_slide_right_in).setClickedListener(this);
        findComponentById(ResourceTable.Id_on_slide_top_in).setClickedListener(this);
        findComponentById(ResourceTable.Id_on_zoom_in).setClickedListener(this);
        findComponentById(ResourceTable.Id_on_roll_in).setClickedListener(this);
        findComponentById(ResourceTable.Id_on_fade_in).setClickedListener(this);
        findComponentById(ResourceTable.Id_on_rubber).setClickedListener(this);
        findComponentById(ResourceTable.Id_on_Path).setClickedListener(this);
        findComponentById(ResourceTable.Id_on_SvgPath).setClickedListener(this);
        findComponentById(ResourceTable.Id_on_Follower).setClickedListener(this);
        findComponentById(ResourceTable.Id_on_Bubble).setClickedListener(this);


    }

    @Override
    public void onClick(Component component) {

        switch (component.getId()) {
            case ResourceTable.Id_on_dialog:
                CommonDialog commonDialog = new CommonDialog(getContext());
                Component com = LayoutScatter.getInstance(getContext()).parse(ResourceTable.Layout_common_dialog, null, true);
                commonDialog.setContentCustomComponent(com);
                com.findComponentById(ResourceTable.Id_ok_tv).setClickedListener(new Component.ClickedListener() {
                    @Override
                    public void onClick(Component component) {
                        commonDialog.destroy();
                    }
                });
                commonDialog.show();
                ViewAnimator.animate(com).slideBottomIn().start();
                break;
            case ResourceTable.Id_on_shake:

                ViewAnimator.animate(component)
                        .shake()
                        .singleInterpolator(Animator.CurveType.LINEAR)
                        .start();
                break;
            case ResourceTable.Id_on_bounce:
                ViewAnimator.animate(component, imageView)
                        .bounce()
                        .singleInterpolator(Animator.CurveType.BOUNCE)
                        .start();
                break;
            case ResourceTable.Id_on_bounce_in:
                ViewAnimator.animate(component, imageView)
                        .bounceIn()
                        .singleInterpolator(Animator.CurveType.BOUNCE)
                        .start();
                break;
            case ResourceTable.Id_on_flash:
                ViewAnimator.animate(component, imageView).flash().start();
                break;
            case ResourceTable.Id_on_flip_orizontal:
                ViewAnimator.animate(component, imageView)
                        .rotation(90, -15, 15, 0)
                        .start();
                break;
            case ResourceTable.Id_on_flip_vertical:
                ViewAnimator.animate(component, imageView)
                        .rotation(90, -15, 15, 0)
                        .start();

//                ViewAnimator.animate(component, imageView).wave().start();
                break;
            case ResourceTable.Id_on_wave:
                ViewAnimator.animate(component, imageView).wave().start();
                break;
            case ResourceTable.Id_on_tada:
                ViewAnimator.animate(component, imageView).tada().start();
                break;
            case ResourceTable.Id_on_pulse:
                ViewAnimator.animate(component, imageView).pulse().start();
                break;
            case ResourceTable.Id_on_stand_up:
                ViewAnimator.animate(component, imageView).standUp().start();
                break;
            case ResourceTable.Id_on_swing:
                ViewAnimator.animate(component, imageView).swing().start();
                break;
            case ResourceTable.Id_on_wobble:
                ViewAnimator.animate(component, imageView).wobble().start();
                break;
            case ResourceTable.Id_on_fall:
                ViewAnimator.animate(component, imageView).fall().start();
                break;
            case ResourceTable.Id_on_news_paper:
                ViewAnimator.animate(component, imageView).newsPaper().start();
                break;
            case ResourceTable.Id_on_slit:
                ViewAnimator.animate(component, imageView).slit().start();
                break;
            case ResourceTable.Id_on_slide_left_in:
                ViewAnimator.animate(component, imageView).slideLeftIn().start();
                break;
            case ResourceTable.Id_on_slide_right_in:
                ViewAnimator.animate(component, imageView).slideRightIn().start();
                break;
            case ResourceTable.Id_on_slide_top_in:
                ViewAnimator.animate(component, imageView).slideTopIn().start();
                break;
            case ResourceTable.Id_on_zoom_in:
                ViewAnimator.animate(component, imageView).zoomIn().start();
                break;
            case ResourceTable.Id_on_roll_in:
                ViewAnimator.animate(component, imageView).rollIn().start();
                break;
            case ResourceTable.Id_on_fade_in:
                ViewAnimator.animate(component, imageView).fadeIn().start();
                break;
            case ResourceTable.Id_on_rubber:
                ViewAnimator.animate(component, imageView).rubber().start();
                break;
            case ResourceTable.Id_on_Path:
                onPath(component);
                break;
            case ResourceTable.Id_on_SvgPath:
                onSvgPath(component);
                break;
            case ResourceTable.Id_on_Follower:
                onFollower(component);
                break;
            case ResourceTable.Id_on_Bubble:
                onBubble(component);
                break;
        }
    }

    public void onFollower(Component view) {
        ViewAnimator.animate(imageView4follower)
                .path(PathUtils.createFollower(metrics.width, metrics.height))
                .repeatCount(Animator.INFINITE)
                .start();
    }

    public void onSvgPath(Component view) {
        final int[] imgLoc = new int[2];
        //save location
        imageView4svg.getLocationOnScreen();
        ViewAnimator.animate(imageView4svg)
                .path(SvgPathParser.tryParsePath(SVG_PATH))
                .repeatCount(2)
                .duration(1500)
                .onStop(new AnimationListener.Stop() {
                    @Override
                    public void onStop() {
                        //restore location
                        imageView4svg.setTranslationX(imgLoc[0]);
                        imageView4svg.setTranslationY(imgLoc[1]);
                    }
                }).start();
    }

    public void onBubble(final Component view) {
        bubbleLayout.removeAllComponents();
        for (ViewAnimator animator : animators) {
            animator.cancel();
        }
        animators.clear();
        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                getAbility().getUITaskDispatcher().asyncDispatch(new Runnable() {
                    @Override
                    public void run() {
                        addHeart();
                    }
                });
            }
        }, 0, 200);
    }

    ArrayList<ViewAnimator> animators = new ArrayList<ViewAnimator>();
    Timer timer = null;

    private void addHeart() {
        HiLog.info(label, "addHeart, size: " + animators.size());
        if (animators.size() == 50) {
            if (timer != null)
                timer.cancel();
            timer = null;
            return;
        }
        final Image imageView = new Image(this);
        imageView.setScaleMode(Image.ScaleMode.INSIDE);
        imageView.setPixelMap(ResourceTable.Media_heart);
        bubbleLayout.addComponent(imageView);
        AnimationBuilder builder = ViewAnimator.animate(imageView);
        builder.path(PathUtils.createBubble(metrics.width, metrics.height));
        builder.fadeOut();
        builder.duration(5000);
        builder.repeatCount(1);
        builder.interpolator(Animator.CurveType.LINEAR);
        ViewAnimator animator = builder.start();
        animators.add(animator);
    }

    public float getdisAttributes(float dp) {
        DisplayAttributes displayAttributes = null;
        Optional<Display> displayOpt = DisplayManager.getInstance().getDefaultDisplay(getContext());
        if (displayOpt.isPresent()) {
            Display display = displayOpt.get();
            displayAttributes = display.getRealAttributes();

        }
        return dp * displayAttributes.densityPixels;
    }

    public void onPath(Component view) {
        //see http://blog.csdn.net/tianjian4592/article/details/47067161
        Path path = new Path();
        path.moveTo(START_POINT[0], START_POINT[1]);
        path.quadTo(RIGHT_CONTROL_POINT[0], RIGHT_CONTROL_POINT[1], BOTTOM_POINT[0], BOTTOM_POINT[1]);
        path.quadTo(LEFT_CONTROL_POINT[0], LEFT_CONTROL_POINT[1], START_POINT[0], START_POINT[1]);
        path.close();
        ViewAnimator.animate(imageView4path)
                .path(path)
//                .repeatCount(Animator.INFINITE)
                .start();
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
