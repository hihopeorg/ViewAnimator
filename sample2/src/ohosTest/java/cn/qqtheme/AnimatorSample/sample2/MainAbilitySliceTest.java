package cn.qqtheme.AnimatorSample.sample2;


import cn.qqtheme.AnimatorSample.EventHelper;
import cn.qqtheme.AnimatorSample.MainAbility;
import cn.qqtheme.AnimatorSample.ResourceTable;
import ohos.aafwk.ability.Ability;
import ohos.agp.components.Button;
import org.junit.After;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class MainAbilitySliceTest {
    @After
    public void clearAbility() {
        EventHelper.clearAbilities();
    }

    @Test
    public void onStart() {
        MainAbility mainAbility = EventHelper.startAbility(MainAbility.class);
        Button button = (Button) mainAbility.findComponentById(ResourceTable.Id_on_dialog);
        onClick(mainAbility, button, 3000);
        Button shake = (Button) mainAbility.findComponentById(ResourceTable.Id_on_shake);
        onClick(mainAbility, shake, 3000);
        Button bounce = (Button) mainAbility.findComponentById(ResourceTable.Id_on_bounce);
        onClick(mainAbility, bounce, 3000);
        EventHelper.inputSwipe(mainAbility,0,400,0,100,2000);
        sleep(2000);
        Button bounce_in = (Button) mainAbility.findComponentById(ResourceTable.Id_on_bounce_in);
        onClick(mainAbility, bounce_in, 3000);
        Button on_flash = (Button) mainAbility.findComponentById(ResourceTable.Id_on_flash);
        onClick(mainAbility, on_flash, 3000);
        Button flip_orizontal = (Button) mainAbility.findComponentById(ResourceTable.Id_on_flip_orizontal);
        onClick(mainAbility, flip_orizontal, 3000);
        Button flip_vertical = (Button) mainAbility.findComponentById(ResourceTable.Id_on_flip_vertical);
        onClick(mainAbility, flip_vertical, 3000);
        EventHelper.inputSwipe(mainAbility,0,700,0,100,2000);
        sleep(2000);
        Button wave = (Button) mainAbility.findComponentById(ResourceTable.Id_on_wave);
        onClick(mainAbility, wave, 3000);
        Button tada = (Button) mainAbility.findComponentById(ResourceTable.Id_on_tada);
        onClick(mainAbility, tada, 3000);
        Button pulse = (Button) mainAbility.findComponentById(ResourceTable.Id_on_pulse);
        onClick(mainAbility, pulse, 3000);
        Button stand_up = (Button) mainAbility.findComponentById(ResourceTable.Id_on_stand_up);
        onClick(mainAbility, stand_up, 3000);
        EventHelper.inputSwipe(mainAbility,0,700,0,100,2000);
        sleep(2000);
        Button swing = (Button) mainAbility.findComponentById(ResourceTable.Id_on_swing);
        onClick(mainAbility, swing, 3000);
        Button wobble = (Button) mainAbility.findComponentById(ResourceTable.Id_on_wobble);
        onClick(mainAbility, wobble, 3000);
        Button fall = (Button) mainAbility.findComponentById(ResourceTable.Id_on_fall);
        onClick(mainAbility, fall, 3000);
        Button news_paper = (Button) mainAbility.findComponentById(ResourceTable.Id_on_news_paper);
        onClick(mainAbility, news_paper, 3000);
        EventHelper.inputSwipe(mainAbility,0,700,0,100,2000);
        sleep(2000);
        Button slit = (Button) mainAbility.findComponentById(ResourceTable.Id_on_slit);
        onClick(mainAbility, slit, 3000);
        Button slide_left_in = (Button) mainAbility.findComponentById(ResourceTable.Id_on_slide_left_in);
        onClick(mainAbility, slide_left_in, 3000);
        Button slide_right_in = (Button) mainAbility.findComponentById(ResourceTable.Id_on_slide_right_in);
        onClick(mainAbility, slide_right_in, 3000);
        EventHelper.inputSwipe(mainAbility,0,700,0,100,2000);
        sleep(2000);
        Button slide_top_in = (Button) mainAbility.findComponentById(ResourceTable.Id_on_slide_top_in);
        onClick(mainAbility, slide_top_in, 3000);
        Button zoom_in = (Button) mainAbility.findComponentById(ResourceTable.Id_on_zoom_in);
        onClick(mainAbility, zoom_in, 3000);
        Button roll_in = (Button) mainAbility.findComponentById(ResourceTable.Id_on_roll_in);
        EventHelper.inputSwipe(mainAbility,0,500,0,100,2000);
        sleep(2000);
        onClick(mainAbility, roll_in, 3000);
        Button fade_in = (Button) mainAbility.findComponentById(ResourceTable.Id_on_fade_in);
        onClick(mainAbility, fade_in, 3000);
        Button on_rubber = (Button) mainAbility.findComponentById(ResourceTable.Id_on_rubber);
        onClick(mainAbility, on_rubber, 3000);
        EventHelper.inputSwipe(mainAbility,0,700,0,100,2000);
        sleep(2000);
        Button on_Path = (Button) mainAbility.findComponentById(ResourceTable.Id_on_Path);
        onClick(mainAbility, on_Path, 3000);
        Button on_SvgPath = (Button) mainAbility.findComponentById(ResourceTable.Id_on_SvgPath);
        onClick(mainAbility, on_SvgPath, 3000);
        EventHelper.inputSwipe(mainAbility,0,700,0,100,2000);
        sleep(2000);
        Button on_Follower = (Button) mainAbility.findComponentById(ResourceTable.Id_on_Follower);
        onClick(mainAbility, on_Follower, 3000);
        Button on_Bubble = (Button) mainAbility.findComponentById(ResourceTable.Id_on_Bubble);
        onClick(mainAbility, on_Bubble, 3000);
    }

    public void onClick(Ability ability, Button button, long time) {
        assertNotNull(button);
        EventHelper.triggerClickEvent(ability, button);
        sleep(time);
    }

    public void sleep(long time) {
        try {
            Thread.sleep(time);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}