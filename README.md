# ViewAnimator

**本项目是基于开源项目ViewAnimator进行ohos化的移植和开发的，可以通过项目标签以及github地址（ https://github.com/florent37/ViewAnimator ）追踪到原项目版本**

#### 项目介绍

- 项目名称：流畅的动画库
- 所属系列：ohos的第三方组件移植
- 功能：动画
- 项目移植状态：完成80%
- 调用差异：无
- 项目作者和维护人：hihope
- 联系方式：hihope@hoperun.com
- 原项目Doc地址：https://github.com/florent37/ViewAnimator
- 原项目基线版本：v1.0.3,sha1:19a4aa49bd5025e4ba51b7f71e89522f3c3018bd
- 编程语言：Java
- 外部库依赖：无
#### 效果演示
<img src="screenshot/sample.gif"/> <img src="screenshot/sample2.gif"/>


#### 安装教程

方法1.

1. 编译依赖库har包ViewAnimator.har。
2. 启动 DevEco Studio，将编译的har包，导入工程目录“entry->libs”下。
3. 在moudle级别下的build.gradle文件中添加依赖，在dependences标签中增加对libs目录下har包的引用。

```
dependencies {
    implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
	……
}
```

4. 在导入的har包上点击右键，选择“Add as Library”对包进行引用，选择需要引用的模块，并点击“OK”即引用成功。

方法2.

1. 在工程的build.gradle的allprojects中，添加HAR所在的Maven仓地址

```
repositories {
    maven {
        url 'http://106.15.92.248:8081/repository/Releases/' 
    }
}
```

2. 在应用模块的build.gradle的dependencies闭包中，添加如下代码:

```
dependencies {
    implementation 'com.github.florent37.ohos:viewanimator:1.0.1'
}
```

#### 使用说明

用一个方法动画多个视图

```java
ViewAnimator
       .animate(image)
            .translationY(-1000, 0)
            .alpha(0,1)
       .andAnimate(text)
            .dp().translationX(-20, 0)
            .decelerate()
            .duration(2000)
       .thenAnimate(image)
            .scale(1f, 0.5f, 1f)
            .accelerate()
            .duration(1000)
       .start();
       
```

在多个视图中添加相同的动画
```java
ViewAnimator
       .animate(image,text)
       .scale(0,1)
       .start();
```

添加监听器
```java
ViewAnimator
       .animate(image)
       .scale(0,1)
       .onStart(() -> {})
       .onStop(() -> {})
       .start();

```

使用VP值
```java
ViewAnimator
       .animate(image)
       .dp().translationY(-200, 0)
       .start();
```

动画高度/宽度
```java
ViewAnimator
       .animate(view)
       .waitForHeight() //wait until a ViewTreeObserver notification
       .dp().width(100,200)
       .dp().height(50,100)
       .start();
```

变色动画
```java
ViewAnimator
       .animate(view)
       .textColor(Color.BLACK,Color.GREEN)
       .backgroundColor(Color.WHITE,Color.BLACK)
       .start();
```

旋转的动画
```java
ViewAnimator
       .animate(view)
       .rotation(360)
       .start();
```

自定义动画
```java
ViewAnimator
       .animate(text)
       .custom(new AnimationListener.Update<TextView>() {
            @Override public void update(TextView view, float value) {
                  view.setText(String.format("%.02f",value));
            }
        }, 0, 1)
       .start();
```
撤销动画
```java
ViewAnimator viewAnimator = ViewAnimator
       .animate(view)
       .rotation(360)
       .start();

viewAnimator.cancel();
```
```java
.shake().interpolator(new LinearInterpolator());
.bounceIn().interpolator(new BounceInterpolator());
.flash().repeatCount(4);
.flipHorizontal();
.wave().duration(5000);
.tada();
.pulse();
.standUp();
.swing();
.wobble();
```


#### 版本迭代


- v1.0.1 
  
#### 版权和许可信息
- Apache Licence

