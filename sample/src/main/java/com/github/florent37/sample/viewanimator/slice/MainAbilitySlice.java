package com.github.florent37.sample.viewanimator.slice;


import com.github.florent37.sample.viewanimator.ResourceTable;

import com.github.florent37.viewanimator.AnimationListener;
import com.github.florent37.viewanimator.ViewAnimator;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.animation.Animator;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.agp.components.Text;
import ohos.agp.utils.Color;
import ohos.agp.window.dialog.ToastDialog;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

import java.util.Locale;
import java.util.logging.Handler;

public class MainAbilitySlice extends AbilitySlice {
    private Image image,mountain;
    private Text percent,text;
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        HiLog.info(new HiLogLabel(HiLog.INFO,0x45857,"MainAbility"),"onStart");
        image= (Image) findComponentById(ResourceTable.Id_image);
        mountain= (Image) findComponentById(ResourceTable.Id_mountain);
        percent= (Text) findComponentById(ResourceTable.Id_percent);
        text= (Text) findComponentById(ResourceTable.Id_text);
        findComponentById(ResourceTable.Id_parallel).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                animateParallel();
            }
        });

        findComponentById(ResourceTable.Id_mountain).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                simpleAnimation();
            }
        });

        findComponentById(ResourceTable.Id_sequentially).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                animateSequentially();
            }
        });
    }
    protected void simpleAnimation() {
        ViewAnimator.animate(mountain)
                .dp().translationY(-1000, 0)
                .alpha(0, 1)
                .andAnimate(text)
                .translationX(-200, 0)
                .interpolator(Animator.CurveType.DECELERATE)
                .duration(2000)

                .thenAnimate(mountain)
                .scale(1f, 0.5f, 1f)
                .interpolator(Animator.CurveType.ACCELERATE)
                .duration(1000)

                .start();
    }

    protected void animateParallel() {
        final ViewAnimator viewAnimator = ViewAnimator.animate(mountain, image)
                .dp().translationY(-1000, 0)
                .alpha(0, 1)
                .singleInterpolator(Animator.CurveType.OVERSHOOT)

                .andAnimate(percent)
                .scale(0, 1.2f)

                .andAnimate(text)
                .textColor(Color.BLACK.getValue(), Color.WHITE.getValue(),Color.BLACK.getValue())
                .backgroundColor(Color.WHITE.getValue(), Color.BLACK.getValue())

                .waitForHeight()
                .singleInterpolator(Animator.CurveType.ACCELERATE_DECELERATE)
                .duration(2000)

                .thenAnimate(percent)
                .custom(new AnimationListener.Update<Text>() {
                    @Override
                    public void update(Text view, float value) {
                        view.setText(String.format(Locale.US, "%.02f%%", value));
                    }
                }, 0, 1)

                .andAnimate(image)
                .rotation(0, 360)

                .duration(5000)
                .start();

        new EventHandler(EventRunner.getMainEventRunner()).postSyncTask(new Runnable() {
            @Override
            public void run() {
                viewAnimator.cancel();
                new ToastDialog(getContext()).setText("animator canceled");
            }
        });
    }


    protected void animateSequentially() {
        ViewAnimator.animate(image)
                .dp().width(100f, 150f)
                .alpha(1, 0.1f)
                .interpolator(Animator.CurveType.DECELERATE)
                .duration(3000)
                .thenAnimate(image)
                .dp().width(150f, 100f)
                .alpha(0.1f, 1f)
                .interpolator(Animator.CurveType.ACCELERATE)
                .duration(1200)
                .start();
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
