package com.github.florent37.sample.viewanimator.sample;

import com.github.florent37.sample.viewanimator.EventHelper;
import com.github.florent37.sample.viewanimator.MainAbility;
import com.github.florent37.sample.viewanimator.ResourceTable;
import ohos.agp.components.Button;
import ohos.agp.components.Image;
import org.junit.After;
import org.junit.Test;

import java.util.Timer;

import static org.junit.Assert.*;

public class MainAbilitySliceTest {
    @After
    public void clearAbility() {
        EventHelper.clearAbilities();
    }
    @Test
    public void onStart() {
        MainAbility mainAbility = EventHelper.startAbility(MainAbility.class);
        Image image= (Image) mainAbility.findComponentById(ResourceTable.Id_mountain);
        Button parallel= (Button) mainAbility.findComponentById(ResourceTable.Id_parallel);
        Button sequentially= (Button) mainAbility.findComponentById(ResourceTable.Id_sequentially);
        Sleep(2000);
        EventHelper.triggerClickEvent(mainAbility,image);
        assertNotNull(image);
        Sleep(5000);
        EventHelper.triggerClickEvent(mainAbility,parallel);
        assertNotNull(parallel);
        Sleep(10000);
        EventHelper.triggerClickEvent(mainAbility,sequentially);
        assertNotNull(sequentially);
        Sleep(10000);

    }
    public void Sleep(long time){
        try {
            Thread.sleep(time);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}