/**
 * Copyright 2015 florent37, Inc.
 *
 *     Licensed under the Apache License, Version 2.0 (the "License");
 *     you may not use this file except in compliance with the License.
 *     You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *     Unless required by applicable law or agreed to in writing, software
 *     distributed under the License is distributed on an "AS IS" BASIS,
 *     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *     See the License for the specific language governing permissions and
 *     limitations under the License.
 * */

package com.github.florent37.viewanimator;

import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorGroup;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.Component;

import ohos.agp.components.ComponentTreeObserver;
import java.util.ArrayList;
import java.util.List;

public class ViewAnimator {
    private static final long DEFAULT_DURATION = 3000L;
    private ViewAnimator prev = null;
    private ViewAnimator next = null;
    private AnimatorGroup animatorSet;

    private int interpolator = -1;
    private int runSet = 1;
    private int repeatCount = 1;
    private AnimationListener.Start startListener;
    private AnimationListener.Stop stopListener;
    private Component waitForThisViewHeight = null;
    List<AnimationBuilder> animationList = new ArrayList<>();
    private long duration = 3000;
    private long startDelay = 0;

    public AnimationBuilder thenAnimate(Component views) {
        ViewAnimator nextViewAnimator = new ViewAnimator();
        this.next = nextViewAnimator;
        nextViewAnimator.prev = this;
        return nextViewAnimator.addAnimationBuilder(views);
    }


    public static AnimationBuilder animate(Component... components) {
        ViewAnimator viewAnimator = new ViewAnimator();
        return viewAnimator.addAnimationBuilder(components);
    }
    public AnimationBuilder addAnimationBuilder(Component... views) {
        AnimationBuilder animationBuilder = new AnimationBuilder(this, views);
        animationList.add(animationBuilder);
        return animationBuilder;
    }
    protected AnimatorGroup createAnimatorSet() {
        List<Animator> animators = new ArrayList<>();
        for (AnimationBuilder animationBuilder : animationList) {
            List<AnimatorValue> list = animationBuilder.createAnimators();
            if (animationBuilder.getSingleInterpolator() != -1) {
                for (AnimatorValue animator : list) {
                    animator.setCurveType(animationBuilder.getSingleInterpolator());
                }
            }
            animators.addAll(list);
        }
        for (AnimationBuilder animationBuilder : animationList) {
            if (animationBuilder.isWaitForHeight()) {
                waitForThisViewHeight = animationBuilder.getView();
                break;
            }
        }
        AnimatorGroup animatorSet = new AnimatorGroup();
        Animator[] animators1 = new Animator[animators.size()];
        for (int i = 0; i < animators.size(); i++) {
            animators1[i] = animators.get(i);
        }
        animatorSet.runParallel(animators1);
        animatorSet.setDuration(duration);
        animatorSet.setDelay(startDelay);
        if (repeatCount != 1)
            animatorSet.setLoopedCount(repeatCount);
        animatorSet.setStateChangedListener(new Animator.StateChangedListener() {
            @Override
            public void onStart(Animator animator) {
                if (startListener != null) startListener.onStart();
            }

            @Override
            public void onStop(Animator animator) {
                if (stopListener != null) stopListener.onStop();
                if (next != null) {
                    next.prev = null;
                     next.start();
                }
            }

            @Override
            public void onCancel(Animator animator) {

            }

            @Override
            public void onEnd(Animator animator) {

            }

            @Override
            public void onPause(Animator animator) {

            }

            @Override
            public void onResume(Animator animator) {

            }
        });
        return animatorSet;
    }

    /**
     * -1 or INFINITE will repeat forever
     */
    public ViewAnimator repeatCount(int repeatCount) {
        this.repeatCount = repeatCount;
        return this;
    }

    public void cancel() {
        if (animatorSet != null) {
            animatorSet.cancel();
        }
        if (next != null) {
            next.cancel();
            next = null;
        }
    }

    public ViewAnimator duration(long duration) {
        this.duration = duration;
        return this;
    }

    public ViewAnimator startDelay(long startDelay) {
        this.startDelay = startDelay;
        return this;
    }

    /**
     * see https://github.com/cimi-chen/EaseInterpolator
     */
    public ViewAnimator interpolator(int interpolator) {
        this.interpolator = interpolator;
        return this;
    }

    /**
     * see https://github.com/cimi-chen/EaseInterpolator
     */
    public ViewAnimator runSet(int interpolator) {
        this.runSet = interpolator;
        return this;
    }

    public ViewAnimator onStop(AnimationListener.Stop stopListener) {
        this.stopListener = stopListener;
        return this;
    }

    public void start() {
        if (prev != null) {
            prev.start();
        } else {
            animatorSet = createAnimatorSet();
            animatorSet.start();
            if (waitForThisViewHeight != null) {
                waitForThisViewHeight.getComponentTreeObserver().addWindowBoundListener(new ComponentTreeObserver.WindowBoundListener() {
                    @Override
                    public void onWindowBound() {
                        animatorSet.start();
                        waitForThisViewHeight.getComponentTreeObserver().removeWindowBoundListener(this);
                    }
                    @Override
                    public void onWindowUnbound() {

                    }
                });
            } else {
                animatorSet.start();
            }
        }
    }
}
