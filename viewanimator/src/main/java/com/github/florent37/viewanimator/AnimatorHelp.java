
/**
 *Copyright (c) 2021 Huawei Device Co., Ltd.
 *
 *     Licensed under the Apache License, Version 2.0 (the "License");
 *     you may not use this file except in compliance with the License.
 *     You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *     Unless required by applicable law or agreed to in writing, software
 *     distributed under the License is distributed on an "AS IS" BASIS,
 *     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *     See the License for the specific language governing permissions and
 *     limitations under the License.
 *
 * */
package com.github.florent37.viewanimator;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.Component;


import java.text.DecimalFormat;


public class AnimatorHelp {
    final static DecimalFormat df = new DecimalFormat("0.00");

    public static class Keyframe {
        float progress;
        float value;

        public Keyframe(float progress, float value) {
            this.progress = progress;
            this.value = value;
        }

        public static Keyframe ofFloat(float progress, float value) {
            return new Keyframe(progress, value);
        }
    }

    public static class PropertyValuesHolder {
        String property;
        Keyframe[] frames;

        public PropertyValuesHolder(String property, Keyframe... frames) {
            this.property = property;
            this.frames = frames;
        }

        public static PropertyValuesHolder ofKeyframe(String property, Keyframe... frames) {
            return new PropertyValuesHolder(property, frames);
        }
    }

    public static PropertyValuesHolder onf(String property, float... values) {

        Keyframe[] keyframes = new Keyframe[values.length];
        for (int i = 0; i < values.length; i++) {
            float pro = (float) (i+1) / values.length;
            keyframes[i] = Keyframe.ofFloat(Float.parseFloat(df.format(pro)), values[i]);
        }
        return new PropertyValuesHolder(property, keyframes);
    }

    public static AnimatorValue getPropertyAnimator(Component component, String property, Keyframe... frames) {
        return getPropertyAnimator(component, new PropertyValuesHolder(property, frames));
    }

    public static AnimatorValue getPropertyAnimator(final Component component, PropertyValuesHolder... holders) {

        AnimatorValue animatorValue = new AnimatorValue();

        animatorValue.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
            @Override
            public void onUpdate(AnimatorValue animatorValue, float v) {
                for (PropertyValuesHolder holder : holders) {
                    for (int i = 0; i < holder.frames.length; i++) {
                        float value;
                        float value2;
                        float progress = i==0?0:holder.frames[i-1].progress;//动画进度
                        float incremental = i == 0 ? holder.frames[i].progress : holder.frames[i].progress - holder.frames[i - 1].progress;//增量
                        if(v>=progress&&v<=(holder.frames[i].progress)) {
                            float Incremental_p = (v - progress) / incremental;//增量进度
                            value = i == 0 ? 0 : holder.frames[i - 1].value;
                            value2 = holder.frames[i].value;
                            switch (holder.property) {
                                case "scaleX":
                                    component.setScaleX(value + (value2 - value) * Incremental_p);
                                    break;
                                case "scaleY":
                                    component.setScaleY(value + (value2 - value) * Incremental_p);
                                    break;
                                case "alpha":
                                    component.setAlpha(value + (value2 - value) * Incremental_p);
                                    break;
                                case "translationX":
                                    component.setTranslationX(value + (value2 - value) * Incremental_p);
                                    break;
                                case "translationY":
                                    component.setTranslationY(value + (value2 - value) * Incremental_p);
                                    break;
                                case "rotation":
                                    component.setRotation(value + (value2 - value) * Incremental_p);
                                    break;
                                case "pivotX":
                                    component.setPivotX(value2);
                                    break;
                                case "pivotY":
                                    component.setPivotY(value2);
                                    break;

                            }
                        }
                    }
                }
            }
        });
        return animatorValue;
    }
}
